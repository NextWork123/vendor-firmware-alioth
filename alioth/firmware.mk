PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/alioth/abl.img:install/firmware-update/abl.img \
    vendor/xiaomi-firmware/alioth/aop.img:install/firmware-update/aop.img \
    vendor/xiaomi-firmware/alioth/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/xiaomi-firmware/alioth/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/xiaomi-firmware/alioth/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/xiaomi-firmware/alioth/devcfg.img:install/firmware-update/devcfg.img \
    vendor/xiaomi-firmware/alioth/dsp.img:install/firmware-update/dsp.img \
    vendor/xiaomi-firmware/alioth/featenabler.img:install/firmware-update/featenabler.img \
    vendor/xiaomi-firmware/alioth/hyp.img:install/firmware-update/hyp.img \
    vendor/xiaomi-firmware/alioth/imagefv.img:install/firmware-update/imagefv.img \
    vendor/xiaomi-firmware/alioth/keymaster.img:install/firmware-update/keymaster.img \
    vendor/xiaomi-firmware/alioth/modem.img:install/firmware-update/modem.img \
    vendor/xiaomi-firmware/alioth/qupfw.img:install/firmware-update/qupfw.img \
    vendor/xiaomi-firmware/alioth/uefisecapp.img:install/firmware-update/uefisecapp.img \
    vendor/xiaomi-firmware/alioth/xbl.img:install/firmware-update/xbl.img \
    vendor/xiaomi-firmware/alioth/xbl_config.img:install/firmware-update/xbl_config.img
